<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function registrasi(){
        return view('register');
    }

    public function success(Request $request){
        $namadepan = $request['nama_depan'];
        $namabelakang = $request['nama_belakang'];
        
        return view('welcome', compact('namadepan', 'namabelakang'));
    }
}
