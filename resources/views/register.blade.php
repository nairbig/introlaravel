<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <h2>Form Biodata</h2>

    <form action="/welcome" method="POST">

        @csrf
        <label>Nama Depan : </label> <br>
        <input type="text" name="nama_depan" placeholder="first name" required> <br>
        
        <label>Nama Belakang :</label> <br>
        <input type="text" name="nama_belakang" placeholder="last name">
        <br> <br>

        <label>Gender : </label> <br>
        <input type="radio" value="m" name="gender"> Male <br>
        <input type="radio" value="fm" name="gender"> Female <br>
        <input type="radio" value="o" name="gender"> Other <br> <br>

        <label>Nationality : </label>
        <select name="kwn">
            <option value="id">Indonesian</option>
            <option value="en">United Kingdom</option>
            <option value="fr">France</option>
            <option value="uae">Arab</option>
        </select> <br> <br>

        <label>Language Spoken :</label> <br>
        <input type="checkbox" value="bhs" name="lang"> Bahasa Indonesia <br>
        <input type="checkbox" value="eng" name="lang"> English <br>
        <input type="checkbox" value="frc" name="lang"> French <br>
        <input type="checkbox" value="arab" name="lang"> Arabic <br> <br>

        <label>Bio : </label> <br>
        <textarea name="bio" cols="30" rows="10" required></textarea> <br>

        <input type="submit" value="kirim">

    </form>
</body>
</html>